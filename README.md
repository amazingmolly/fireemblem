# Fire Emblem 2 (GAIDEN)

This is a pure HTML5 game on top of [TypeScript](http://www.typescriptlang.org). Due to some technical reasons, it currently only works on [Google Chrome browser](http://www.google.com/chrome).

### LICENSE ###
[MIT](https://github.com/amazingmolly/fireemblem/blob/master/LICENSE)

### LIVE DEMO ###
[http://amazingmolly.com/fe2/](http://amazingmolly.com/fe2/)

![](https://raw.githubusercontent.com/amazingmolly/fireemblem/master/snapshot/fe.capture.png)